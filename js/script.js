class Contacts {

}

class User {
    constructor (data) {
        this.data = {
            id: data.id,
            fName: data.fName,
            email: data.email,
            address: data.address,
            phone: data.phone,
        }
    }

    set info(newData) {
        this.data = {
            id: newData.id,
            fName: newData.fName,
            email: newData.email,
            address: newData.address,
            phone: newData.phone,
        }
    }

    get info() {
        return this.data;
    }
}

let alex = new User ({id: '123', fName: 'Alex', email: 'alex@mail.ru', address: 'minsk', phone: '+37525511111'});

console.log(alex);
console.log(alex.info);
alex.info = {id: '1223', fName: 'Bob', email: 'alex@mail.ru', address: 'minsk', phone: '+37525511111'};
console.log(alex.info);

